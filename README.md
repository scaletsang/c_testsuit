# A Testsuit for testing C Programs

In preparation for studying in 42, I decided to develop a library for testing C functions and programs. The aim is to spend time ahead designing an easy interface and framework for testing C programs so that I can have a smoother debugging experience with useful debug messages.

## Installation

Download the tar file ([Click here](testsuit.tar)) into a folder you want to test your code.  
Unpack the tar files by typing `tar -xf testsuit.rar`.  
Then you will see the testsuit/ library and an example testcases.c file that shows you an interface of testing your code.

## Pure Tests

1. Comparing a function's return value to an expected value.
2. Comparing a function's return value to the content of a seperate file.

## Effectful Tests (In development)

1. Comparing a function's output to the terminal to an expected string.
2. Comparing a function's output to the terminal to the content of a seperate file.

## Program Tests (In development)

1. Effectful tests to a seperate program.