NAME= pure_tests
DEST= testsuit/
OBJS= ${NAME}.o compare.o printer.o ft_string.o helpers.o
FLAGS= -Wall -Wextra -Werror

all: ${OBJS} zip

${NAME}.o: lib/${NAME}.c
	gcc ${FLAGS} -c lib/${NAME}.c

compare.o: lib/compare.c
	gcc ${FLAGS} -c lib/compare.c

printer.o: lib/printer.c
	gcc ${FLAGS} -c lib/printer.c

ft_string.o: lib/ft_string.c
	gcc ${FLAGS} -c lib/ft_string.c

helpers.o: lib/helpers.c
	gcc ${FLAGS} -c lib/helpers.c

pack:
	mkdir -p testsuit
	cp lib/*.h ${DEST}
	mv *.o ${DEST}

zip: pack
	tar -cvf testsuit.tar testsuit
	tar -rvf testsuit.tar testcases.c

clean:
	rm -f ${DEST}*.o

fclean: clean
	rm -f testsuit.tar
	rm -f ${DEST}*.h

re: fclean all