/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   testcases.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: htsang <htsang@student.42heilbronn.de>     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/07 12:04:30 by htsang            #+#    #+#             */
/*   Updated: 2021/11/15 16:33:15 by htsang           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./testsuit/pure_tests.h"

t_test_result	normal_case(void)
{
	init_test(s)

	subject = "";
	expect.s;
	output.s;
	return (test_result(subject, type, output, expect));
}

t_test_result	case_with_test_file(void)
{
	init_test_ext_file("expected.txt")

	subject = "";
	output.s;
	return (test_result(subject, s, output, file_name));
}

t_test_result	multiple_cases(void)
{
	init_multiple_tests("expected.txt", 50, "\n")

	subject = "";
	outputs;
	output.d = subtests(outputs, file_name, delimiter, max_line_size);
	return (test_result(subject, d, output, 1));
}

int	main(void)
{
	test(&normal_case);
	test_ext_file(&case_with_test_file);
	test(&multiple_cases);
	return (0);
}
