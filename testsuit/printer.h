#ifndef PRINTER_H
#define PRINTER_H

#include "core.h"
#include "color.h"

void	print_output_msg(t_test_result result, char *color);

void	print_success_msg(t_test_result result);

void	print_failure_msg(t_test_result result);

void	print_failure_msg_subtest(char *file_name, int line_c);

void	print_failure_msg_diff_file(int unmatched_range[4]);

void	print_too_short_msg_diff_file(void);

void	print_too_long_msg_diff_file(void);

void	print_malloc_error(t_test_result result);

void	print_file_error(char *file_name);

void	print_test_result(t_test_result result, int (*cmp)(t_io, t_io));

int	print_subtest_result(t_test_result result, char *file_name, int line_c);

void	print_test_result_diff_file(t_test_result result, FILE *fp, int accu_correct);

#endif
