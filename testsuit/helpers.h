#ifndef	HELPERS_H
#define HELPERS_H

#include "ft_string.h"

void	ft_reverse(char *tab, int size);

char	*ft_itoa(int n, char *s);

#endif
