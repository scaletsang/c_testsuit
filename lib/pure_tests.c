#include "pure_tests.h"

void	test(t_test_result (*test_case)(void))
{
	t_test_result	result;

	result = (*test_case)();
	print_test_result(result, select_compare_function(result.return_type));
	return ;
}

void	test_cmp(t_test_result (*test_case)(void), int (*cmp)(t_io, t_io))
{
	t_test_result	result;

	result = (*test_case)();
	print_test_result(result, cmp);
	return ;
}

static void	_test_ext_file(t_test_result result, FILE *fp)
{
	int		unmatched_range[4] = {1, 0, 1, 0};
	int		is_correct;
	int		accu_correct;
	char	c;
	int		i;

	is_correct = 1;
	accu_correct = 1;
	c = fgetc(fp);
	i = 0;
	while (result.output.s[i] && !feof(fp))
	{
		if (c == '\n')
		{
			unmatched_range[0]++;
			unmatched_range[1] = 0;
		}
		if (result.output.s[i] == c)
		{
			if (is_correct)
			{
				unmatched_range[1]++;
			}
			else
			{
				print_failure_msg_diff_file(unmatched_range);
				unmatched_range[3]++;
				unmatched_range[0] = unmatched_range[2];
				unmatched_range[1] = unmatched_range[3];	
			}
			is_correct = 1;
		}
		else
		{
			if (is_correct)
			{
				unmatched_range[1]++;
				unmatched_range[2] = unmatched_range[0];
				unmatched_range[3] = unmatched_range[1];
			}
			else
			{
				unmatched_range[3]++;
			}
			is_correct = 0;
			accu_correct = 0;
		}
		c = fgetc(fp);
		i++;
	}
	if (!is_correct)
	{
		print_failure_msg_diff_file(unmatched_range);
	}
	result.output.s = &result.output.s[i];
	print_test_result_diff_file(result, fp, accu_correct);
	return ;
}

void	test_ext_file(t_test_result (*test_case)(void))
{
	t_test_result	result;
	FILE			*fp;

	result = (*test_case)();
	fp = fopen(result.expect.s, "r");
	if (!fp)
	{
		print_file_error(result.expect.s);
		exit(1);
	}
	_test_ext_file(result, fp);
	fclose(fp);
	return ;
}

// Takes in a list of string and then compare to every line
// in the text file specified in the second argument.
// One line in the file corresponds to one string in the list.
// 
// The normal use case is that the third argument is "\\n" but
// in case of multiple lines output, it is possible to change
// the delimiter.
int	_subtests(char **outputs, char *file_name, char *delimiter, int max_line_size, t_test_result result)
{
	FILE	*fp;
	char	*fscanf_format;
	int		line_c;
	int		accu_correctness = 1;

	fp = fopen(file_name, "r");
	if (!fp)
	{
		print_file_error(file_name);
		fclose(fp);
		return (-1);
	}
	line_c = 1;
	result.expect.s = malloc(sizeof(char) * (max_line_size + 1));
	if (result.expect.s == 0)
	{
		print_malloc_error(result);
		fclose(fp);
		return (-1);
	}
	fscanf_format = malloc(sizeof(char) * (ft_strlen(delimiter) + 5));
	fscanf_format = "%[^";
	ft_strcat(fscanf_format, delimiter);
	ft_strcat(fscanf_format, "]");
	result.expect.s[max_line_size] = 0;
	while (*outputs)
	{
		fscanf(fp, fscanf_format, result.expect.s);
		result.output.s = *outputs;
		accu_correctness = print_subtest_result(result, file_name, line_c);
		line_c++;
		outputs++;
	}
	free(result.expect.s);
	free(fscanf_format);
	fclose(fp);
	return (accu_correctness);
}
