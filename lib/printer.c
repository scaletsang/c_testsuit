#include "printer.h"

void	print_output_msg(t_test_result result, char *color)
{
	char	scan_text[33];

	ft_strcpy(scan_text, color);
	switch (result.return_type)
	{
		case c:
			printf(ft_strcat(scan_text, "Expect: %c\nOutput: %c\n"WHITE), result.expect.c, result.output.c);
			break;
		case d:
			printf(ft_strcat(scan_text, "Expect: %d\nOutput: %d\n"WHITE), result.expect.d, result.output.d);
			break;
		case u:
			printf(ft_strcat(scan_text, "Expect: %u\nOutput: %u\n"WHITE), result.expect.u, result.output.u);
			break;
		case l:
			printf(ft_strcat(scan_text, "Expect: %ld\nOutput: %ld\n"WHITE), result.expect.l, result.output.l);
			break;
		case s:
			printf(ft_strcat(scan_text, "Expect: %s\nOutput: %s\n"WHITE), result.expect.s, result.output.s);
			break;
		case p:
			printf(ft_strcat(scan_text, "Expect: %p\nOutput: %p\n"WHITE), result.expect.p, result.output.p);
			break;
		default:
			printf(RED"Unknown type %c in return type.\n"WHITE, result.return_type);
			break;
	}
	return ;
}

void	print_success_msg(t_test_result result)
{
	printf(GREEN"OK in %s of %s.\n"WHITE, result.func, result.file);
	printf(GREEN"Suceeded subject: %s\n"WHITE, result.subject);
	return ;
}

void	print_failure_msg(t_test_result result)
{
	printf(RED"KO in %s of %s.\n"WHITE, result.func, result.file);
	printf(YELLOW"Failed subject: %s\n"WHITE, result.subject);
	return ;
}

void	print_failure_msg_subtest(char *file_name, int line_c)
{
	printf(RED"KO in subtest. Line %d in file %s\n"WHITE, line_c, file_name);
	return ;
}

void	print_failure_msg_diff_file(int unmatched_range[4])
{
	printf(RED"Unmatched result occured from line %d col %d to line %d col %d.\n"WHITE, unmatched_range[0], unmatched_range[1], unmatched_range[2], unmatched_range[3]);
	return ;
}

void	print_too_short_msg_diff_file(void)
{
	printf(RED"Output string is shorter than expected.\n"WHITE);
	return ;
}

void	print_too_long_msg_diff_file(void)
{
	printf(RED"Output string is longer than expected.\n"WHITE);
	return ;
}

void	print_malloc_error(t_test_result result)
{
	printf(RED"Malloc returned a null pointer. Insufficient space for malloc.\nError occured when allocating memory for comparing results in %s %s.\n"WHITE, result.func, result.file);
	return ;
}

void	print_file_error(char *file_name)
{
	printf(RED"Cannot find the compare file named %s.\n"WHITE, file_name);
	return ;
}

// Takes in a test result and a pointer to a compare function of two IO types.
// Run the compare function over the output and expect value.
// Print a success message if it returns 1, otherwise print a failure message.
void	print_test_result(t_test_result result, int (*cmp)(t_io, t_io))
{
	printf(WHITE"==============\n"WHITE);
	if ((*cmp)(result.expect, result.output))
	{
		print_success_msg(result);
		print_output_msg(result, GREEN);
	}
	else
	{
		print_failure_msg(result);
		print_output_msg(result, YELLOW);
	}
	return ;
}

int	print_subtest_result(t_test_result result, char *file_name, int line_c)
{
	int	is_correct;

	is_correct = 1;
	if (ft_strcmp(result.expect.s, result.output.s) != 0)
	{
		is_correct = 0;
		print_failure_msg_subtest(file_name, line_c);
		print_output_msg(result, YELLOW);
	}
	return (is_correct);
}

void	print_test_result_diff_file(t_test_result result, FILE *fp, int accu_correct)
{
	if (feof(fp))
	{
		if (*result.output.s == 0)
		{
			if (accu_correct)
			{
				print_success_msg(result);
			}
			else
			{
				print_failure_msg(result);
			}
			return ;
		}
		print_too_short_msg_diff_file();
	}
	else
	{
		print_too_long_msg_diff_file();
	}
	print_failure_msg(result);
	return ;
}
