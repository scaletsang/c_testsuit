#ifndef PURE_TESTS_H
#define PURE_TESTS_H

#include "core.h"
#include "compare.h"
#include "printer.h"

#define	init_test(type_enum) \
	char			*subject; \
	t_io			expect; \
	t_io			output; \
	t_return_type	type = type_enum;

#define	init_test_ext_file(file_name_val) \
	char			*subject; \
	char			*file_name = file_name_val; \
	t_io			output; \

#define	init_multiple_tests(file_name_val, max_line_size_val, delimiter_val) \
	init_test_ext_file(file_name_val) \
	int		max_line_size = max_line_size_val; \
	char	*delimiter = delimiter_val; \
	char	**outputs; \

void	test(t_test_result (*test_case)(void));

void	test_cmp(t_test_result (*test_case)(void), int (*cmp)(t_io, t_io));

void	test_ext_file(t_test_result (*test_case)(void));

int	_subtests(char **outputs, char *file_name, char *delimiter, int max_line_size, t_test_result result);

#define subtests(output, file_name, delimiter, line_size) \
	_subtests(output, file_name, delimiter, line_size, empty_result);

#endif
