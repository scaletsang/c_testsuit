#ifndef COLOR_H
#define COLOR_H

#define RED "\033[1;31m"
#define	BLUE "\033[0;34m"
#define GREEN "\033[1;32m"
#define WHITE "\033[0m"
#define YELLOW "\033[1;33m"

#endif