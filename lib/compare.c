#include "compare.h"

int	compare_char(t_io output, t_io expect)
{
	return (output.c == expect.c);
}

int	compare_int(t_io output, t_io expect)
{
	return (output.d == expect.d);
}

int	compare_uint(t_io output, t_io expect)
{
	return (output.u == expect.u);
}

int	compare_long(t_io output, t_io expect)
{
	return (output.l == expect.l);
}

int	compare_str(t_io output, t_io expect)
{
	return (ft_strcmp(output.s, expect.s) == 0);
}

int	compare_pointers(t_io output, t_io expect)
{
	return (output.p == expect.p);
}

int	(*select_compare_function(t_return_type type))(t_io, t_io)
{
	switch (type)
	{
		case c:
			return (&compare_char);
		case d:
			return (&compare_int);
		case u:
			return (&compare_uint);
		case l:
			return (&compare_long);
		case s:
			return (&compare_str);
		case p:
			return (&compare_pointers);
		default:
			return (0);
	}
}

int	compare_char_to_fp(t_io output, FILE *fp)
{
	if (feof(fp))
	{
		return (-1);
	}
	return (output.c == fgetc(fp));
}

int	compare_str_to_fp(t_io output, FILE *fp, char *buffer, int max_line_size)
{
	char	*expect;

	if (feof(fp))
	{
		return (-1);
	}
	expect = fgets(buffer, max_line_size, fp);
	return (ft_strcmp(output.s, expect) == 0);
}
