#ifndef COMPARE_H
#define COMPARE_H

#include "core.h"

int	compare_char(t_io output, t_io expect);

int	compare_int(t_io output, t_io expect);

int	compare_uint(t_io output, t_io expect);

int	compare_long(t_io output, t_io expect);

int	compare_str(t_io output, t_io expect);

int	compare_pointers(t_io output, t_io expect);

int	(*select_compare_function(t_return_type type))(t_io, t_io);

int	compare_char_to_fp(t_io output, FILE *fp);

int	compare_str_to_fp(t_io output, FILE *fp, char *buffer, int max_line_size);

#endif
