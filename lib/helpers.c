#include "helpers.h"

void	ft_reverse(char *tab, int size)
{
	char	*start;
	char	*end;
	char	tmp;
	int		i;

	start = tab;
	end = tab + size - 1;
	i = 0;
	while (i < (size / 2))
	{
		tmp = *start;
		*start = *end;
		*end = tmp;
		start++;
		end--;
		i++;
	}
	return ;
}

char	*ft_itoa(int n, char *s)
{
	int	i;
	int	sign;

    i = 0;
	sign = 1;
    if (n < 0)
	{
		sign = -1;
    	n = -n;
	}
	while (n / 10 > 0)
	{
		s[i] = n % 10 + '0';
		n /= 10;
		i++;
	}
    if (sign < 0)
	{
		s[i] = '-';
		i++;
	}
    s[i] = 0;
    ft_reverse(s, ft_strlen(s));
	return (s);
}
