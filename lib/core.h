#ifndef CORE_H
#define CORE_H

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include "ft_string.h"
#include "helpers.h"

typedef	union u_io
{
	char			c;
	int				d;
	unsigned int	u;
	long			l;
	char			*s;
	void			*p;
} t_io;

typedef enum e_return_type {
	c = 'c',
	d = 'd',
	u = 'u',
	l = 'l',
	s = 's',
	p = 'p'
} t_return_type;

typedef struct s_test_result
{
	const char		*file;
	const char		*func;
	char			*subject;
	t_return_type	return_type;
	t_io			output;
	t_io			expect;
} t_test_result;

#define test_result(subject, type, output, expect) \
	((t_test_result) {__FILE__, __func__, subject, type, output, expect})

#define empty_result \
	((t_test_result) {__FILE__, __func__, 0, s, 0, 0})

#endif