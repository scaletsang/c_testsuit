#ifndef FT_STRING_H
#define FT_STRING_H

int	ft_strlen(char *str);

int	ft_strcmp(char *s1, char *s2);

int	ft_strncmp(char *s1, char *s2, unsigned int n);

char	*ft_strcpy(char *dest, char *src);

char	*ft_strncpy(char *dest, char *src, unsigned int n);

unsigned int	ft_strlcpy(char *dest, char *src, unsigned int size);

char	*ft_strcat(char *dest, char *src);

char	*ft_strncat(char *dest, char *src, unsigned int nb);

unsigned int	ft_strlcat(char *dest, char *src, unsigned int size);

char	*ft_strstr(char *str, char *to_find);

#endif
